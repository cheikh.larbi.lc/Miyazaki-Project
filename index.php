<?php include('templates/mainSection.php') ?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="initial-scale=1.0">
   <link rel="stylesheet" href="./style.css">
   <title>Miyazaki's World</title>
</head>
<body id="index">

   <section id="first">
      <h2><span>Mi</span>yazaki's <span>Wo</span>rld</h2>         
      <div class="box1"></div>
      <div class="loader">
         <span style="--i:0;"></span>
         <span style="--i:1;"></span>
         <span style="--i:2;"></span>
         <span style="--i:3;"></span>
         <a href="heros.php"><img class="arrow" src="images/arrow-left.png" alt=""></a>
      </div>
   </section>